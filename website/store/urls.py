from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.StoreView.as_view(), name='store-home'),
    path('detalhe/<slug>/', views.ProductDetailView.as_view(), name='store-detalhe'),
    path('carrinho/', views.CarrinhoCompraView.as_view(), name='carrinho'),
    path('add-to-cart/<slug>/', views.add_to_cart, name='add_to_cart'),
    path('remove-from-cart/<slug>/', views.remove_from_cart, name='remove_from_cart'),
    path('remove-item-from-cart/<slug>/', views.remove_single_from_cart, name='remove_single_from_cart'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)