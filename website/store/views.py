from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, View
from django.shortcuts import redirect
from django.utils import timezone
from blog.models import Produto, Encomenda, EncomendaProduto
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .form import DataForm

# Create your views here.

class StoreView(ListView):
    model = Produto
    template_name = 'store/home.html'
    context_object_name = 'produtos'


class CarrinhoCompraView(LoginRequiredMixin, View):

    def get(self, *args, **kwargs):
        try:
            form = DataForm()
            encomenda = Encomenda.objects.get(usuario=self.request.user, status=False)
            context = {
                'object': encomenda,
                'form': form
            }
            return render(self.request, 'store/carrinho_de_compras.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("carrinho")
    
    def post(self, *args, **kwargs):
        form = DataForm(self.request.POST or None)
        try:
            encomenda = Encomenda.objects.get(usuario=self.request.user, status=False)
            if form.is_valid():
                data = form.cleaned_data.get('data')
                print("The Form is Valid")
                encomenda.data = data
                total = encomenda.get_total()
                encomenda.precoFinal = total
                encomenda.save()
                return redirect("carrinho")
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("carrinho")


class ProductDetailView(DetailView):
    model = Produto
    template_name = 'store/detalhe_produto.html'

@login_required
def add_to_cart(request, slug):
    produto = get_object_or_404(Produto, slug=slug)
    encomenta_produto, criado = EncomendaProduto.objects.get_or_create(produto=produto, usuario=request.user)
    encomenda_qs = Encomenda.objects.filter(usuario=request.user, status=False)

    if encomenda_qs.exists():
        encomenda = encomenda_qs[0]
        if encomenda.produtos.filter(produto__slug=produto.slug).exists():
            encomenta_produto.quantidade += 1
            encomenta_produto.save()
            total = encomenda.get_total()
            encomenda.precoFinal = total
            encomenda.save()
            return redirect("carrinho")
        else:
            encomenda.produtos.add(encomenta_produto)
            total = encomenda.get_total()
            encomenda.precoFinal = total
            encomenda.save()
            return redirect("carrinho")
    else:
        encomenda_data = timezone.now()
        encomenda = Encomenda.objects.create(usuario=request.user, data=encomenda_data)
        encomenda.produtos.add(encomenta_produto)
        total = encomenda.get_total()
        encomenda.precoFinal = total
        encomenda.save()
    return redirect("carrinho")

@login_required
def remove_from_cart(request, slug):
    produto = get_object_or_404(Produto, slug=slug)
    encomenda_qs = Encomenda.objects.filter(usuario=request.user, status=False)

    if encomenda_qs.exists():
        encomenda = encomenda_qs[0]
        if encomenda.produtos.filter(produto__slug=produto.slug).exists():
            encomenda_produto = EncomendaProduto.objects.filter(produto=produto, usuario=request.user)[0]
            encomenda.produtos.remove(encomenda_produto)
            encomenda_produto.delete()
            total = encomenda.get_total()
            encomenda.precoFinal = total
            encomenda.save()
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("carrinho")
    else:
        messages.info(request, "You do not have an active order")
        return redirect("store-detalhe", slug=slug)
    return redirect("store-detalhe", slug=slug)

@login_required
def remove_single_from_cart(request, slug):
    produto = get_object_or_404(Produto, slug=slug)
    encomenda_qs = Encomenda.objects.filter(usuario=request.user, status=False)

    if encomenda_qs.exists():
        encomenda = encomenda_qs[0]
        if encomenda.produtos.filter(produto__slug=produto.slug).exists():
            encomenda_produto = EncomendaProduto.objects.filter(produto=produto, usuario=request.user)[0]
            if encomenda_produto.quantidade > 1:
                encomenda_produto.quantidade -= 1
                encomenda_produto.save()
                total = encomenda.get_total()
                encomenda.precoFinal = total
                encomenda.save()
            else:
                encomenda.produtos.remove(encomenda_produto)
                encomenda_produto.delete()
                total = encomenda.get_total()
                encomenda.precoFinal = total
                encomenda.save()
            messages.info(request, "This item quantity was updated.")
            return redirect('carrinho')
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("store-detalhe", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("store-detalhe", slug=slug)
    return redirect("store-detalhe", slug=slug)