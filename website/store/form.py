from django import forms
from django.utils import timezone

class DataForm(forms.Form):
    data = forms.DateField(initial=timezone.now())