from django.conf import settings
from django.db import models
from django.shortcuts import reverse

# Create your models here.

class Categoria(models.Model):

    nome = models.CharField(max_length=100)
    descricao = models.TextField(default='Descricao')
    imagem = models.ImageField(default='default.jpg', upload_to='category_pics')
    def __str__(self):
        return self.nome

class Massa(models.Model):

    tipo = models.CharField(max_length=100)

    def __str__(self):
        return self.tipo

class Cobertura(models.Model):

    tipo = models.CharField(max_length=100)

    def __str__(self):
        return self.tipo

class Produto(models.Model):
    
    nome = models.CharField(max_length=100)
    preco = models.FloatField()
    descricao = models.TextField()
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    massa = models.ForeignKey(Massa, on_delete=models.CASCADE)
    cobertura = models.ForeignKey(Cobertura, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='product_pics')
    slug = models.SlugField()

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse('store-detalhe', kwargs={
            'slug': self.slug
        })
    
    def get_add_to_cart_url(self):
        return reverse('add_to_cart', kwargs={
            'slug': self.slug
        })
    
    def get_remove_from_cart_url(self):
        return reverse('remove_from_cart', kwargs={
            'slug': self.slug
        })


class EncomendaProduto(models.Model):

    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    status = models.BooleanField(default=False)
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
    quantidade = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.quantidade} - {self.produto.nome}"

    def get_total_produto_price(self):
        return self.quantidade * self.produto.preco
    """
    def get_total_discount_item_price(self):
        return self.quantidade * self.produto.discount_price

    def get_amount_saved(self):
        return self.get_total_item_price() - self.get_total_discount_item_price()
    """
    def get_final_price(self):
        #if self.item.discount_price:
        #    return self.get_total_discount_item_price()
        return self.get_total_produto_price()


class Encomenda(models.Model):

    usuario = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    produtos = models.ManyToManyField(EncomendaProduto, null=True)
    precoFinal = models.FloatField(null=True)
    data = models.DateField(null=True)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.usuario.username

    def get_total(self):
        total = 0
        for en_produto in self.produtos.all():
            total += en_produto.get_final_price()
        return total
    

"""
class Pagamento(models.Model):
    
    tipo = models.CharField(max_length=100)
    #status = models.IntegerField()
    #data = models.DateField()
    #encomenda = models.ForeignKey(Encomenda, on_delete=models.CASCADE)

class Estado(models.Model):
    nome = models.CharField(max_length=100)
    uf = models.CharField(max_length=2)

class Cidade(models.Model):
    nome = models.CharField(max_length=100)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)

class Bairro(models.Model):
    nome = models.CharField(max_length=100)
    cidade = models.ForeignKey(Cidade, on_delete=models.CASCADE)

class Logradouro(models.Model):
    nome = models.CharField(max_length=100)
    bairro = models.ForeignKey(Bairro, on_delete=models.CASCADE)
"""