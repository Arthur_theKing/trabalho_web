from django.contrib import admin
from .models import  Categoria, Cobertura, Massa, Produto, Encomenda, EncomendaProduto


# Register your models here.
"""
admin.site.register(Logradouro)
admin.site.register(Bairro)
admin.site.register(Cidade)
admin.site.register(Estado)
"""
admin.site.register(Categoria)
admin.site.register(Cobertura)
admin.site.register(Massa)
admin.site.register(Produto)
#admin.site.register(Pagamento)
admin.site.register(Encomenda)
admin.site.register(EncomendaProduto)
