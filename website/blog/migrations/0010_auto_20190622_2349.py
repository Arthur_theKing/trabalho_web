# Generated by Django 2.1.7 on 2019-06-23 02:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_remove_encomenda_produto'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pagamento',
            name='data',
        ),
        migrations.RemoveField(
            model_name='pagamento',
            name='encomenda',
        ),
        migrations.RemoveField(
            model_name='pagamento',
            name='status',
        ),
    ]
