from django.shortcuts import render
from django.http import HttpResponse
from .models import Categoria
# Create your views here.

def home(request):

    contex = {
        'categorias': Categoria.objects.all()
    }

    return render(request, 'blog/index.html', contex)